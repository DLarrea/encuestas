import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { SurveyAddComponent } from './survey-add/survey-add.component';
import { PagesComponent } from './pages.component';


@NgModule({
  declarations: [
    SurveyAddComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
